package c.moviecatalog.ui

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import c.moviecatalog.R
import c.moviecatalog.util.DataBindingIdlingResource
import c.moviecatalog.util.EspressoIdlingResource
import c.moviecatalog.util.monitorActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class NavigationTest {
    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    private val dataBindingIdlingResource = DataBindingIdlingResource()

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
        IdlingRegistry.getInstance().register(dataBindingIdlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
        IdlingRegistry.getInstance().unregister(dataBindingIdlingResource)
    }

    @Test
    fun dashboardToDetailTest() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        // recheck toolbar main is displayed
        onView(withId(R.id.toolbar_main)).check(matches(isDisplayed()))

        // Check Tabs
        onView(withText(R.string.title_tab_movie)).check(matches(isDisplayed()))
        onView(withText(R.string.title_tab_tv_show)).check(matches(isDisplayed()))

        // Click Tabs
        onView(withText(R.string.title_tab_movie)).perform(click())
        onView(withText(R.string.title_tab_tv_show)).perform(click())
        onView(withText(R.string.title_tab_movie)).perform(click())

        // check recycler view movie
        onView(withId(R.id.rv_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_movie)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.rv_movie)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.rv_movie)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.rv_movie)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.rv_movie))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click())
            )

        // check detail
        onView(withId(R.id.layout_detail)).check(matches(isDisplayed()))
        // Swipe up collapsing toolbar
        onView(withId(R.id.appbar_detail)).perform(click(), swipeUp())
        pressBack()

        // check recycler view movie
        onView(withText(R.string.title_tab_tv_show)).check(matches(isDisplayed()))
        onView(withText(R.string.title_tab_tv_show)).perform(click())
        onView(withId(R.id.rv_tv_show)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_tv_show)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.rv_tv_show)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.rv_tv_show)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.rv_tv_show)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.rv_tv_show))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click())
            )

        // check detail
        onView(withId(R.id.layout_detail)).check(matches(isDisplayed()))
        // Swipe up collapsing toolbar
        onView(withId(R.id.appbar_detail)).perform(click(), swipeUp())
        pressBack()

        // Check tabview tv show
        onView(withText(R.string.title_tab_tv_show)).check(matches(isDisplayed()))

        // back to movie tabview
        onView(withText(R.string.title_tab_movie)).perform(click())
        onView(withText(R.string.title_tab_movie)).check(matches(isDisplayed()))
    }

    @Test
    fun appBarMenuNavigationTest() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)

        // recheck toolbar main is displayed
        onView(withId(R.id.toolbar_main)).check(matches(isDisplayed()))

        // check favorite menu
        onView(withId(R.id.favoriteFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.favoriteFragment)).perform(click())
        pressBack()

        // recheck toolbar main is displayed
        onView(withId(R.id.toolbar_main)).check(matches(isDisplayed()))

        // check menu settings
        onView(withId(R.id.menu_settings)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_settings)).perform(click())
        pressBack()

        // recheck toolbar main is displayed
        onView(withId(R.id.toolbar_main)).check(matches(isDisplayed()))

        // check button search, button favorite page, button settings page
        onView(withId(R.id.searchFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.favoriteFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_settings)).check(matches(isDisplayed()))

        // check search menu
        onView(withId(R.id.searchFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.searchFragment)).perform(click())
        pressBack()
    }
}