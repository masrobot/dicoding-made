package c.moviecatalog.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import c.moviecatalog.database.contenttvshowsearch.ContentTvShowSearchDao
import c.moviecatalog.database.contenttvshowsearch.asDomainModel
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.network.Services
import c.moviecatalog.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContentTvShowSearchRepository(private val contentTvShowSearchDao: ContentTvShowSearchDao) {
    val contentTvShowSearch: LiveData<List<ContentResult>> =
        Transformations.map(contentTvShowSearchDao.getAllContentTvShowSearch().toLiveData(pageSize = 20)) {
            it.asDomainModel()
        }

    suspend fun getTvShowSearch(query: String) {
        withContext(Dispatchers.IO) {
            val contentList = Services().getSearchTvShows(query)
            contentTvShowSearchDao.updateData(*contentList.asDatabaseModel())
        }
    }
}