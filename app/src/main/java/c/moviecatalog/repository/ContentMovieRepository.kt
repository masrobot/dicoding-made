package c.moviecatalog.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import c.moviecatalog.database.contentmovie.ContentMovieDao
import c.moviecatalog.database.contentmovie.asDomainModel
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.network.Services
import c.moviecatalog.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContentMovieRepository(private val contentMovieDao: ContentMovieDao) {
    val contentMovie: LiveData<List<ContentResult>> =
        Transformations.map(contentMovieDao.getAllContentMovie().toLiveData(pageSize = 10)) {
            it.asDomainModel()
        }

    suspend fun refreshContentMovie() {
        withContext(Dispatchers.IO) {
            try {
                val contentList = Services().getMovies()
                contentMovieDao.updateData(*contentList.asDatabaseModel())
            } catch (e: Exception) {
                // if error load data from API, load data from local
                Log.e("Error", e.localizedMessage as String)
                contentMovie
            }
        }
    }
}