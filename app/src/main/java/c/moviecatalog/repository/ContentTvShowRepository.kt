package c.moviecatalog.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import c.moviecatalog.database.contenttvshow.ContentTvShowDao
import c.moviecatalog.database.contenttvshow.asDomainModel
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.network.Services
import c.moviecatalog.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContentTvShowRepository(private val contentTvShowDao: ContentTvShowDao) {
    val contentTvShow: LiveData<List<ContentResult>> =
        Transformations.map(contentTvShowDao.getAllContentTvShow().toLiveData(pageSize = 10)) {
        it.asDomainModel()
    }

    suspend fun refreshContentTvShow() {
        withContext(Dispatchers.IO) {
            try {
                val contentList = Services().getTvShows()
                contentTvShowDao.updateData(*contentList.asDatabaseModel())
            } catch (e: Exception) {
                // if error load data from API, load data from local
                Log.e("Error", e.localizedMessage as String)
                contentTvShow
            }
        }
    }
}