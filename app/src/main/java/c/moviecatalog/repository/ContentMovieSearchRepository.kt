package c.moviecatalog.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import c.moviecatalog.database.contentmoviesearch.ContentMovieSearchDao
import c.moviecatalog.database.contentmoviesearch.asDomainModel
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.network.Services
import c.moviecatalog.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContentMovieSearchRepository(private val contentMovieSearchDao: ContentMovieSearchDao) {
    val contentMovieSearch: LiveData<List<ContentResult>> =
        Transformations.map(contentMovieSearchDao.getAllContentMovieSearch().toLiveData(pageSize = 20)) {
            it.asDomainModel()
        }

    suspend fun getMovieSearch(query: String) {
        withContext(Dispatchers.IO) {
            val contentList = Services().getSearchMovies(query)
            contentMovieSearchDao.updateData(*contentList.asDatabaseModel())
        }
    }
}