package c.moviecatalog.repository

import c.moviecatalog.database.contentmovieupcoming.ContentMovieUpcomingDao
import c.moviecatalog.database.contentmovieupcoming.ContentMovieUpcomingEntity
import c.moviecatalog.network.Services
import c.moviecatalog.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ContentMovieUpcomingRepository(private val contentMovieUpcomingDao: ContentMovieUpcomingDao) {
    suspend fun getMovieUpcomingByDate(today: String): List<ContentMovieUpcomingEntity> {
        return withContext(Dispatchers.IO) {
            contentMovieUpcomingDao.getContentMovieUpcoming(today)
        }
    }

    suspend fun refreshMovieUpcoming() {
        withContext(Dispatchers.IO) {
            val contentList = Services().getMovieUpcoming()
            contentMovieUpcomingDao.updateData(*contentList.asDatabaseModel())
        }
    }
}