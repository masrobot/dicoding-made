package c.moviecatalog.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import c.moviecatalog.database.favorite.FavoriteDao
import c.moviecatalog.database.favorite.FavoriteEntity
import c.moviecatalog.database.favorite.asDomainModel
import c.moviecatalog.domain.ContentResult


class FavoriteRepository(private val favoriteDao: FavoriteDao) {
    fun getAllMovieFavorite(): LiveData<List<ContentResult>> =
        Transformations.map(favoriteDao.getAllFavoriteMovie().toLiveData(pageSize = 5)) {
            it.asDomainModel()
        }

    fun getAllTvShowFavorite(): LiveData<List<ContentResult>> =
        Transformations.map(favoriteDao.getAllFavoriteTvShow().toLiveData(pageSize = 5)) {
            it.asDomainModel()
        }

    fun getFavoriteById(id: Int): FavoriteEntity? = favoriteDao.getFavoriteById(id)

    @WorkerThread
    suspend fun deleteFavoriteById(id: Int) = favoriteDao.deleteFavoriteById(id)

    @WorkerThread
    suspend fun insert(favorite: FavoriteEntity) = favoriteDao.insert(favorite)
}