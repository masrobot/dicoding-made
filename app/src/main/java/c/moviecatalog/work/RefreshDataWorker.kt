package c.moviecatalog.work

import android.app.Application
import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import c.moviecatalog.repository.ContentMovieRepository
import c.moviecatalog.repository.ContentMovieUpcomingRepository
import c.moviecatalog.repository.ContentTvShowRepository
import org.koin.android.ext.android.inject

class RefreshDataWorker(appContext: Context, params: WorkerParameters) : CoroutineWorker(appContext, params) {
    companion object {
        const val WORK_NAME = "RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        val contentMovieRepository: ContentMovieRepository by Application().inject()
        val contentTvShowRepository: ContentTvShowRepository by Application().inject()
        val contentMovieUpcomingRepository: ContentMovieUpcomingRepository by Application().inject()

        return try {
            contentMovieRepository.refreshContentMovie()
            contentTvShowRepository.refreshContentTvShow()
            contentMovieUpcomingRepository.refreshMovieUpcoming()

            Result.success()
        } catch (e: Throwable) {
            Result.retry()
        }
    }
}