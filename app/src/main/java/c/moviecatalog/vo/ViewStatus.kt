package c.moviecatalog.vo

enum class ViewStatus { LOADING, ERROR, DONE, SEARCH }
