package c.moviecatalog.database.contenttvshowsearch

import androidx.paging.DataSource
import androidx.room.*

@Dao
interface ContentTvShowSearchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg contentTvShowSearch: ContentTvShowSearchEntity)

    @Query("SELECT * FROM content_tv_show_search_table ORDER BY _id ASC")
    fun getAllContentTvShowSearch(): DataSource.Factory<Int, ContentTvShowSearchEntity>

    @Query("DELETE FROM content_tv_show_search_table")
    suspend fun deleteAllContentTvShowSearch()

    @Transaction
    suspend fun updateData(vararg contentTvShowSearch: ContentTvShowSearchEntity) {
        deleteAllContentTvShowSearch()
        insert(*contentTvShowSearch)
    }
}