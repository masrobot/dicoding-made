package c.moviecatalog.database

import androidx.room.Database
import androidx.room.RoomDatabase
import c.moviecatalog.database.contentmovie.ContentMovieDao
import c.moviecatalog.database.contentmovie.ContentMovieEntity
import c.moviecatalog.database.contentmoviesearch.ContentMovieSearchDao
import c.moviecatalog.database.contentmoviesearch.ContentMovieSearchEntity
import c.moviecatalog.database.contentmovieupcoming.ContentMovieUpcomingDao
import c.moviecatalog.database.contentmovieupcoming.ContentMovieUpcomingEntity
import c.moviecatalog.database.contenttvshow.ContentTvShowDao
import c.moviecatalog.database.contenttvshow.ContentTvShowEntity
import c.moviecatalog.database.contenttvshowsearch.ContentTvShowSearchDao
import c.moviecatalog.database.contenttvshowsearch.ContentTvShowSearchEntity
import c.moviecatalog.database.favorite.FavoriteDao
import c.moviecatalog.database.favorite.FavoriteEntity

@Database(
    entities = [
        ContentMovieEntity::class,
        ContentTvShowEntity::class,
        FavoriteEntity::class,
        ContentMovieUpcomingEntity::class,
        ContentMovieSearchEntity::class,
        ContentTvShowSearchEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class MovieCatalogDatabase : RoomDatabase() {
    abstract fun contentMovieDao(): ContentMovieDao
    abstract fun contentTvShowDao(): ContentTvShowDao
    abstract fun favoriteDao(): FavoriteDao
    abstract fun contentMovieUpcomingDao(): ContentMovieUpcomingDao
    abstract fun contentMovieSearchDao(): ContentMovieSearchDao
    abstract fun contentTvShowSearchDao(): ContentTvShowSearchDao
}