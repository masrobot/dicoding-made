package c.moviecatalog.database.favorite

import android.database.Cursor
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favorite: FavoriteEntity)

    @Query("SELECT * FROM favorite_table ORDER BY _id DESC")
    fun getAllFavorite(): List<FavoriteEntity>

    @Query("SELECT * FROM favorite_table WHERE isMovie = 1 ORDER BY _id DESC")
    fun getAllFavoriteMovie(): DataSource.Factory<Int, FavoriteEntity>

    @Query("SELECT * FROM favorite_table WHERE isTvShow = 1 ORDER BY _id DESC")
    fun getAllFavoriteTvShow(): DataSource.Factory<Int, FavoriteEntity>

    @Query("SELECT * FROM favorite_table WHERE id = :id")
    fun getFavoriteById(id: Int): FavoriteEntity?

    @Query("DELETE FROM favorite_table WHERE id = :id")
    suspend fun deleteFavoriteById(id: Int)

    @Query("SELECT * FROM favorite_table ORDER BY _id DESC")
    fun readFavoriteCursor(): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContentProvider(favorite: FavoriteEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllContentProvider(favorite: Array<FavoriteEntity>): LongArray

    @Query("DELETE FROM favorite_table WHERE id = :id")
    fun deleteContentProviderById(id: Long): Int

}