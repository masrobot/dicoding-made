package c.moviecatalog.di

import androidx.room.Room
import c.moviecatalog.database.MovieCatalogDatabase
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.*
import c.moviecatalog.ui.detail.DetailViewModel
import c.moviecatalog.ui.movieui.movie.MovieViewModel
import c.moviecatalog.ui.movieui.moviefavorite.MovieFavoriteViewModel
import c.moviecatalog.ui.movieui.moviesearch.MovieSearchViewModel
import c.moviecatalog.ui.search.SearchViewModel
import c.moviecatalog.ui.tvshowui.tvshow.TvShowViewModel
import c.moviecatalog.ui.tvshowui.tvshowfavorite.TvShowFavoriteViewModel
import c.moviecatalog.ui.tvshowui.tvshowsearch.TvShowSearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val roomTestModule = module {
    single {
        Room.inMemoryDatabaseBuilder(get(), MovieCatalogDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }
    single { get<MovieCatalogDatabase>().contentMovieDao() }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            MovieCatalogDatabase::class.java,
            "Dicoding MADE MovieCatalogDatabase"
        ).fallbackToDestructiveMigration()
            .build()
    }
    single { get<MovieCatalogDatabase>().contentMovieDao() }
    single { get<MovieCatalogDatabase>().contentMovieSearchDao() }
    single { get<MovieCatalogDatabase>().contentMovieUpcomingDao() }
    single { get<MovieCatalogDatabase>().contentTvShowDao() }
    single { get<MovieCatalogDatabase>().contentTvShowSearchDao() }
    single { get<MovieCatalogDatabase>().favoriteDao() }
}

val viewModelModule = module {
    viewModel { MovieViewModel(get()) }
    viewModel { TvShowViewModel(get()) }
    viewModel { MovieFavoriteViewModel(get()) }
    viewModel { TvShowFavoriteViewModel(get()) }
    viewModel { (contentData: ContentResult, isMovie: Boolean, isTvShow: Boolean) ->
        DetailViewModel(
            contentData,
            get(),
            isMovie,
            isTvShow
        )
    }
    viewModel { SearchViewModel(get(), get()) }
    viewModel { MovieSearchViewModel(get()) }
    viewModel { TvShowSearchViewModel(get()) }
}

val repositoryModule = module {
    single { ContentMovieRepository(get()) }
    single { ContentMovieSearchRepository(get()) }
    single { ContentMovieUpcomingRepository(get()) }
    single { ContentTvShowRepository(get()) }
    single { ContentTvShowSearchRepository(get()) }
    single { FavoriteRepository(get()) }
}