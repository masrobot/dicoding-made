package c.moviecatalog.ui.tvshowui.tvshow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.ContentTvShowRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class TvShowViewModel(private val contentTvShowRepository: ContentTvShowRepository) : ViewModel() {
    var contentTvShow: LiveData<List<ContentResult>> = contentTvShowRepository.contentTvShow

    private val _tvShows = MutableLiveData<List<ContentResult>>()
    val tvShows: LiveData<List<ContentResult>>
        get() = _tvShows

    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    private val _refreshStatus = MutableLiveData<Boolean>()
    val refreshStatus: LiveData<Boolean>
        get() = _refreshStatus

    private val _navigateToDetail = MutableLiveData<ContentResult>()
    val navigateToDetail: LiveData<ContentResult>
        get() = _navigateToDetail

    init {
        getTvShowList()
    }

    fun displayDetail(contentResult: ContentResult) {
        _navigateToDetail.value = contentResult
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    fun onRefresh() {
        _refreshStatus.value = false
        getTvShowList()
    }

    private fun getTvShowList() {
        viewModelScope.launch {
            contentTvShowRepository.refreshContentTvShow()
        }
    }

    fun contentTvShowData(contentTvShow: List<ContentResult>) {
        if (contentTvShow.isNullOrEmpty()) {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.ERROR
        } else {
            _statusConnectionView.value = ViewStatus.LOADING
            _tvShows.value = contentTvShow
            _statusConnectionView.value = ViewStatus.DONE
            _refreshStatus.value = false
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}