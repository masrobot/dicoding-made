package c.moviecatalog.ui.tvshowui.tvshowfavorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.FavoriteRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel

class TvShowFavoriteViewModel(private val favoriteRepository: FavoriteRepository) : ViewModel() {
    lateinit var tvShowFavoriteList: LiveData<List<ContentResult>>

    private val _tvShowFavorites = MutableLiveData<List<ContentResult>>()
    val tvShowFavorites: LiveData<List<ContentResult>>
        get() = _tvShowFavorites

    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    private val _refreshStatus = MutableLiveData<Boolean>()
    val refreshStatus: LiveData<Boolean>
        get() = _refreshStatus

    private val _navigateToDetail = MutableLiveData<ContentResult>()
    val navigateToDetail: LiveData<ContentResult>
        get() = _navigateToDetail

    init {
        getTvShowFavorite()
    }

    private fun getTvShowFavorite() {
        tvShowFavoriteList = favoriteRepository.getAllTvShowFavorite()
        _refreshStatus.value = false
    }

    fun tvShowFavoriteData(favorite: List<ContentResult>) {
        if (favorite.isNullOrEmpty()) {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.ERROR
            _refreshStatus.value = false
        } else {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.DONE
            _refreshStatus.value = false
            _tvShowFavorites.value = favorite
        }
    }

    fun displayDetail(favorite: ContentResult) {
        _navigateToDetail.value = favorite
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    fun onRefresh() {
        _refreshStatus.value = false
        getTvShowFavorite()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}