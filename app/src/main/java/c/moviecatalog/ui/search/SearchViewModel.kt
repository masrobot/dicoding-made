package c.moviecatalog.ui.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.repository.ContentMovieSearchRepository
import c.moviecatalog.repository.ContentTvShowSearchRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchViewModel(
    private val contentMovieSearchRepository: ContentMovieSearchRepository,
    private val contentTvShowSearchRepository: ContentTvShowSearchRepository
) : ViewModel() {
    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    init {
        _statusConnectionView.value = ViewStatus.SEARCH
    }

    fun onQuery(value: String?): Boolean {
        _statusConnectionView.value = ViewStatus.LOADING
        viewModelScope.launch {
            value?.let {
                delay(300)
                if (it.isEmpty() || it.isBlank()) {
                    _statusConnectionView.value = ViewStatus.SEARCH
                } else {
                    try {
                        contentMovieSearchRepository.getMovieSearch(it)
                        contentTvShowSearchRepository.getTvShowSearch(it)
                        _statusConnectionView.value = ViewStatus.DONE
                    } catch (e: Throwable) {
                        Log.e("Error", e.localizedMessage as String)
                        _statusConnectionView.value = ViewStatus.ERROR
                    }
                }
            }
        }
        return true
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}