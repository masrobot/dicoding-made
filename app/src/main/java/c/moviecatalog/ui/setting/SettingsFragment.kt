package c.moviecatalog.ui.setting

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import c.moviecatalog.R
import c.moviecatalog.receiver.DailyNotificationReceiver
import c.moviecatalog.receiver.ReleaseNotificationReceiver

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val dailyNotificationReceiver = DailyNotificationReceiver()
    private val releaseNotificationReceiver = ReleaseNotificationReceiver()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        val chooseLanguage = findPreference<Preference>("choose_language")
        chooseLanguage?.intent = Intent(Settings.ACTION_LOCALE_SETTINGS)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            "release_reminder" -> {
                when (sharedPreferences?.getBoolean(key, false)) {
                    true -> releaseNotificationReceiver.setReleaseAlarm(context as Context)
                    false -> releaseNotificationReceiver.cancelAlarmRelease(context as Context)
                }
            }
            "daily_reminder" -> {
                when (sharedPreferences?.getBoolean(key, false)) {
                    true -> dailyNotificationReceiver.setDailyAlarm(context as Context)
                    false -> dailyNotificationReceiver.cancelAlarmDaily(context as Context)
                }
            }
        }
    }

    // SharedPreferences Listener Lifecycle
    override fun onPause() {
        super.onPause()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }
}