package c.moviecatalog.ui.favorite


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import c.moviecatalog.R
import c.moviecatalog.adapter.TabViewPagerAdapter
import c.moviecatalog.ui.movieui.moviefavorite.MovieFavoriteFragment
import c.moviecatalog.ui.tvshowui.tvshowfavorite.TvShowFavoriteFragment
import kotlinx.android.synthetic.main.fragment_favorite.*

class FavoriteFragment : Fragment() {
    private lateinit var tabViewPagerAdapter: TabViewPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_favorite.setupWithNavController(findNavController())

        tabViewPagerAdapter = TabViewPagerAdapter(childFragmentManager).apply {
            addFragment(MovieFavoriteFragment(), resources.getString(R.string.title_tab_movie))
            addFragment(TvShowFavoriteFragment(), resources.getString(R.string.title_tab_tv_show))
        }
        view_pager_favorite.adapter = tabViewPagerAdapter
    }

}
