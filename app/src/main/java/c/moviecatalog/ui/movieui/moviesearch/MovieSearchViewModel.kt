package c.moviecatalog.ui.movieui.moviesearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.ContentMovieSearchRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel

class MovieSearchViewModel(contentMovieSearchRepository: ContentMovieSearchRepository) : ViewModel() {
    var contentMovie: LiveData<List<ContentResult>> = contentMovieSearchRepository.contentMovieSearch

    private val _movies = MutableLiveData<List<ContentResult>>()
    val movies: LiveData<List<ContentResult>>
        get() = _movies

    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    private val _navigateToDetail = MutableLiveData<ContentResult>()
    val navigateToDetail: LiveData<ContentResult>
        get() = _navigateToDetail

    fun displayDetail(contentResult: ContentResult) {
        _navigateToDetail.value = contentResult
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    fun contentMovieData(contentMovie: List<ContentResult>) {
        if (contentMovie.isNullOrEmpty()) {
            _statusConnectionView.value = ViewStatus.SEARCH
        } else {
            _statusConnectionView.value = ViewStatus.LOADING
            _movies.value = contentMovie
            _statusConnectionView.value = ViewStatus.DONE
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}