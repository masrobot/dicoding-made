package c.moviecatalog.ui.movieui.moviefavorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.FavoriteRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel

class MovieFavoriteViewModel(private val favoriteRepository: FavoriteRepository) : ViewModel() {
    lateinit var movieFavoriteList: LiveData<List<ContentResult>>

    private val _movieFavorites = MutableLiveData<List<ContentResult>>()
    val movieFavorites: LiveData<List<ContentResult>>
        get() = _movieFavorites

    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    private val _refreshStatus = MutableLiveData<Boolean>()
    val refreshStatus: LiveData<Boolean>
        get() = _refreshStatus

    private val _navigateToDetail = MutableLiveData<ContentResult>()
    val navigateToDetail: LiveData<ContentResult>
        get() = _navigateToDetail

    init {
        getMovieFavorite()
    }

    private fun getMovieFavorite() {
        movieFavoriteList = favoriteRepository.getAllMovieFavorite()
        _refreshStatus.value = false
    }

    fun movieFavoriteData(favorite: List<ContentResult>) {
        if (favorite.isNullOrEmpty()) {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.ERROR
            _refreshStatus.value = false
        } else {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.DONE
            _refreshStatus.value = false
            _movieFavorites.value = favorite
        }
    }

    fun displayDetail(favorite: ContentResult) {
        _navigateToDetail.value = favorite
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    fun onRefresh() {
        _refreshStatus.value = false
        getMovieFavorite()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}