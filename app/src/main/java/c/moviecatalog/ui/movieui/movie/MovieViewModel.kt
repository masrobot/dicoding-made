package c.moviecatalog.ui.movieui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.moviecatalog.domain.ContentResult
import c.moviecatalog.repository.ContentMovieRepository
import c.moviecatalog.vo.ViewStatus
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MovieViewModel(private val contentMovieRepository: ContentMovieRepository) : ViewModel() {
    var contentMovie: LiveData<List<ContentResult>> = contentMovieRepository.contentMovie

    private val _movies = MutableLiveData<List<ContentResult>>()
    val movies: LiveData<List<ContentResult>>
        get() = _movies

    private val _statusConnectionView = MutableLiveData<ViewStatus>()
    val statusConnectionView: LiveData<ViewStatus>
        get() = _statusConnectionView

    private val _refreshStatus = MutableLiveData<Boolean>()
    val refreshStatus: LiveData<Boolean>
        get() = _refreshStatus

    private val _navigateToDetail = MutableLiveData<ContentResult>()
    val navigateToDetail: LiveData<ContentResult>
        get() = _navigateToDetail

    init {
        getMovieList()
    }

    fun displayDetail(contentResult: ContentResult) {
        _navigateToDetail.value = contentResult
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    fun onRefresh() {
        _refreshStatus.value = false
        getMovieList()
    }

    private fun getMovieList() {
        viewModelScope.launch {
            contentMovieRepository.refreshContentMovie()
        }
    }

    fun contentMovieData(contentMovie: List<ContentResult>) {
        if (contentMovie.isNullOrEmpty()) {
            _statusConnectionView.value = ViewStatus.LOADING
            _statusConnectionView.value = ViewStatus.ERROR
        } else {
            _statusConnectionView.value = ViewStatus.LOADING
            _movies.value = contentMovie
            _statusConnectionView.value = ViewStatus.DONE
            _refreshStatus.value = false
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}