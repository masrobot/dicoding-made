package c.moviecatalog.movie

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.toLiveData
import androidx.room.Room
import c.moviecatalog.database.MovieCatalogDatabase
import c.moviecatalog.database.contentmovie.ContentMovieDao
import c.moviecatalog.database.contentmovie.ContentMovieEntity
import c.moviecatalog.repository.ContentMovieRepository
import c.moviecatalog.ui.movieui.movie.MovieViewModel
import c.moviecatalog.util.LiveDataTestUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var context: Context

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private lateinit var movieCatalogDatabase: MovieCatalogDatabase
    private lateinit var contentMovieDao: ContentMovieDao
    private lateinit var contentMovieRepository: ContentMovieRepository
    private lateinit var movieViewModel: MovieViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        movieCatalogDatabase = Room.inMemoryDatabaseBuilder(
            context,
            MovieCatalogDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
        contentMovieDao = movieCatalogDatabase.contentMovieDao()
        contentMovieRepository = ContentMovieRepository(contentMovieDao)
        movieViewModel = MovieViewModel(contentMovieRepository)
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        Dispatchers.resetMain()
        movieCatalogDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun testMovieContent() = runBlocking {
        val contentData = ContentMovieEntity()
        contentData.apply {
            backdropPath = "Backdrop"
            adult = false
            id = 9999
        }
        contentMovieDao.testInsert(contentData)

        val content = contentMovieDao.getAllContentMovie()
        assertFalse(LiveDataTestUtil.getValue(content.toLiveData(pageSize = 10)).isEmpty())
    }
}